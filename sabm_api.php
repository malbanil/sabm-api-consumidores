<?php

define("_SABM_URL",   "http://sabmlatmdcdev.prod.acquia-sites.com/gfsaab");
define("_SABM_USER",  "SABM PROVIDED");
define("_SABM_TOKEN", "SABM PROVIDED");

class sabm_api {

  public $url;
  public $user;
  public $token;

  public function __construct(){
    $this->url = _SABM_URL;
    $this->user = _SABM_USER;
    $this->token = _SABM_TOKEN;
  }

  public function connectApi($method = NULL,$params = NULL){
    $ch = curl_init();
    if($method != NULL) curl_setopt($ch, CURLOPT_URL,$this->url.'/'.$method);
    $str_paranms = '';
    if($params != NULL){
       foreach ($params as $key => $value) {
        $str_paranms .= '&' . $key . '=' . $value;
      }
    }
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"api_user="._SABM_USER."&api_token="._SABM_TOKEN.$str_paranms);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    curl_close ($ch);
    $this->validate_response(json_decode($server_output)->response->id);
    return json_decode($server_output)->response;
  }

  function formId() {
    $method = 'get-params';
    $params  = array('param'=>'formid');
    $response_api = $this->connectApi($method,$params);
    $reponse = $response_api->params[0]->formid;
    return $reponse;
  }

  function formData(){
    $method = 'get-params';
    $params  = array('param'=>'all');
    $response_api = $this->connectApi($method,$params);
    $reponse = $response_api->params;
    return $reponse;
  }

  function location($country,$level){
    $method = 'get-location';
    $params  = array('country'=>$country,'level'=>$level);
    $response_api = $this->connectApi($method,$params);
    $reponse = $response_api->location[0];
    return $reponse;
  }

  function validate_response($id){
    switch ($id) {
      case 100:
      echo "error";
      exit();
      break;
      case 107:
      echo "error";
      break;
      case 109:
      echo "error";
      break;
    }

  }
}
?>
