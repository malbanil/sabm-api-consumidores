<?php

//Include library
include ('sabm_api.php');
$sabm_object = new sabm_api();
$formData = $sabm_object->formData();

//Format fields
$form_data['formId'] = $formData[0]->all->formid;
$form_data['brandTid'] = $formData[0]->all->brand->tid;
$form_data['brandName'] = $formData[0]->all->brand->name;
$form_data['countryTid'] = $formData[0]->all->country->tid;
$form_data['countryName'] = $formData[0]->all->country->name;
$form_data['countryResource'] = $formData[0]->all->country->url_resource;

$listAllJson = file_get_contents($form_data['countryResource']);
$listDepCity = json_decode($listAllJson);

print '<script languaje="JavaScript">
        var varListDepCity= '.$listAllJson.';
      </script>';

//HTML Deparments
$listDepartmentsJson = file_get_contents('http://sabmlatmdcdev.prod.acquia-sites.com/callback-department/'.$form_data['countryName'].'/0');
$listDepartments = json_decode($listDepartmentsJson);

$htmlDepartments = '<div class="city">
        <label>Departamento:</label>
        <input type="hidden" name="city" id="sabmform-state" value="">
        <select name="city_select" id="edit-tmpstate">
        <option selected="selected" value="_empty_">-- Seleccione una opcion --</option>';

foreach ($listDepartments as $key => $value) {
  $htmlDepartments .= '<option value="'.$key.'">'.$value.'</option>';
}
$htmlDepartments .= '</select></div>';

//HTML Cities
$listCitiesJson = file_get_contents('http://sabmlatmdcdev.prod.acquia-sites.com/callback-department/'.$form_data['countryName'].'/1');
$listCities = json_decode($listCitiesJson);

$htmlCities = '<div class="state">
        <label>Ciudad:</label>
        <input type="hidden" id="sabmform-city" name="state" value="">
        <select name="state_select" id="edit-tmpcity">';
foreach ($listCities as $key => $value) {
  $htmlCities .= '<option value="'.$key.'">'.$value.'</option>';
}
$htmlCities .= '</select></div>';

//Html  render field channel options
$statusChannel = $formData[0]->all->channel;
$channel_list = $formData[0]->all->channel_list;
if($statusChannel){
  $htmlChannel = '<div class="channel-list"><label>Channel List:</label><select name="combosChannel">';
  foreach ($channel_list as $key => $value) {
    $htmlChannel .= '<option value="' . $key . '">'. $value . '</option>';
  }
  $htmlChannel .= '</select></div>';
}else{
  $htmlChannel = '<input type="hidden" name="combosChannel" value="null">';
}

//Html render field selection question.
if( !empty($formData[0]->all->question) AND !empty($formData[0]->all->answers) ){
  $question = $formData[0]->all->question;
  $answers = $formData[0]->all->answers;
  $answers_array = explode(',', $answers);
  $htmlQuestions = '<div class="questions">
                      <input type="hidden" name="name_question_one" value="'. $question . '">
                      <label>'. $question . '</label><br>';
  foreach ($answers_array as $key => $value) {
    if(!empty($value)){
      $htmlQuestions .=  '<input type="checkbox" name="answer_question_one" value="'. $value .'">'. $value .'<br>';
    }
  }
  $htmlQuestions .= '</div>';
}else{
  $htmlQuestions = '<input type="hidden" name="name_question_one" value="null">
                    <input type="hidden" name="answer_question_one" value="null">';
}

//Html render seccion redes socials
if(isset($formData[0]->all->require_socialinfo) AND $formData[0]->all->require_socialinfo == '1'){
  $htmlSocialRedes = '<div class="facebook">
                        <label>Facebook:</label>
                        <input type="text" name="facebook-id" value="">
                      </div>
                      <div class="twitter">
                        <label>Twitter:</label>
                        <input type="text" name="twitter" value="">
                      </div>';
}else{
  $htmlSocialRedes = '<input type="hidden" name="facebook-id" value="null">
                      <input type="hidden" name="twitter" value="null">';
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="form_example.js" type="text/javascript"></script>
  <title>Form Example</title>
</head>
<body>
  <div>
    <h2>Form Example</h2>
    <form action="<?php print _SABM_URL; ?>/set-data" method="post" style=" width:auto;">
      <input type="hidden" name="api_user" value="<?php print _SABM_USER; ?>">
      <input type="hidden" name="api_token" value="<?php print _SABM_TOKEN; ?>">
      <input type="hidden" name="campaing" value="UTM-NACIONAL">
      <input type="hidden" name="brand" value="<?php print $form_data['brandName']; ?>">
      <input type="hidden" name="formid" value="<?php print $form_data['formId']; ?>">
      <div class="name">
        <label>Name:</label>
        <input type="text" name="name" value="">
      </div>
      <div class="last-name">
        <label>Last Name:</label>
        <input type="text" name="last-name" value="">
      </div>
      <?php
      print $htmlChannel;
      print $htmlQuestions;
      ?>
      <div class="open-question">
        <input type="hidden" name="name_question_two" value="Cómo quieres que nos comuniquemos contigo?">
        <label>Cómo quieres que nos comuniquemos contigo?</label>
        <input type="text" name="answer_question_two" value="">
      </div>
      <div class="birth-day">
        <label>Birth Day:</label>
        <input type="text" name="birth-day" value="" placeholder="23/09/1990">
      </div>
      <div class="country">
        <label>País:</label>
          <input type="hidden" name="country" value="<?php print $form_data['countryName']; ?>">
          <select name="country_select">
            <option value="<?php print $form_data['countryName']; ?>" disabled="disabled"><?php print $form_data['countryName']; ?></option>
          </select>
      </div>
      <?php
        print "<div class='file_resource' style='margin: 10px 0px;'><label style='border-style: solid;'>Archivo de Recurso para departamento y ciudad: " . $form_data['countryResource'] . "</label></div>";
        print $htmlDepartments;
        print $htmlCities;
      ?>
      <div class="email">
        <label>Email:</label>
        <input type="text" name="email" value="">
      </div>
      <div class="phone">
        <label>Phone:</label>
        <input type="text" name="phone" value="">
      </div>
      <div class="optin">
        <label>Acepta recibir informacion de la marca?</label>
        <input type="checkbox" name="optin" value="1">
      </div>
      <div class="gender">
        <label>Sexo:</label>
        <input type="radio" name="gender" value="Masculino">M
        <input type="radio" name="gender" value="Femenino">F
      </div>
      <?php print $htmlSocialRedes; ?>
    <div class="tyc">
      <label>Terminos y condiciones:</label>
      <input type="checkbox" name="tyc" value="1">
    </div>
    <div>
      <input type="submit" name="submit" value="send">
    </div>
  </form>
</div>
</body>
</html>
