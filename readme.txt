//Readme
V1.0  misael.albanil@vml.com  Date(10/08/2015)
Inicio de construccion de libreria en PHP que facilite la implementacion con el API de consumidores de SAB Miller.
Metodos desarrollados:
  1. formId   : Retorna el id del formulario configurado.
  2. formData : Retorna toda la informacion de configuracion de un formulario. 
  3. location : Retorna la informacion de localizacion de un pais.